// list
// san mateo: 593
// hayward: 579
// redwood city: 548
// santa clara: 632
// san jose: 516
// fremont: 644
// santa teresa: 668
// los gatos: 640

var request = require('request');
var request = request.defaults({jar: true});// enable cookie

var officeList = ['593', '579', '548', '632', '516', '644', '668', '640'];
var officeDict = {
    '593': 'san mateo',
    '579': 'hayward',
    '548': 'redwood city',
    '632': 'santa clara',
    '516': 'san jose',
    '644': 'fremont',
    '668': 'santa teresa',
    '640': 'los gatos'
};
function myForm(office){
  return {
        'numberItems': 1,
        'officeId': office,
        'requestedTask': 'DT',
        'firstName': 'John', // to be modified
        'lastName':'Doe', // to be modified
        'dlNumber':'F1234567', // to be modified
        'birthMonth':'01', // to be modified
        'birthDay':'19', // to be modified
        'birthYear':'1990', // to be modified
        'telArea': '650', // to be modified
        'telPrefix': '123', // to be modified
        'telSuffix': '4567', // to be modified
        'resetCheckFields':true
        };
}
var nodemailer = require('nodemailer'),
    config = require('./config'),
    smtpTransport = nodemailer.createTransport(config.mail.from);

const noTestMonth = ', September'; // to be modified
const testMonths = [', July', ', August']; // to be modified
const noTestDates = ['4', '8']; // to be modified
const startDate = '27'; // to be modified
const endDate = '15'; // to be modified
const noStartHours = ['8', '9', '10']; // to be modified
const refreshInterval = 30000; // 30 secs  // to be modified

function sendMail(officeId, month, dmvDate, myText) {
    var mailOptions = {
        from: [config.mail.name, config.mail.from.auth.user].join(' '),
        to: config.mail.to.join(','),
        subject: "office: "+ officeDict[officeId] + " has test. Date: "+month +" " + dmvDate,
        text: myText
    };

    smtpTransport.sendMail(mailOptions, function(error, response){
        if (error) {
            console.log(error);
        } else {
            console.log('Message sent: ' + response.message);
        }
        smtpTransport.close();
    });
};

function checkoffice(office){
    var newJar = request.jar();
    request({
        uri: 'https://www.dmv.ca.gov/wasapp/foa/findDriveTest.do',
        method: 'POST',
        timeout: 10000,
        form: myForm(office),
        headers: {
                     'Content-Type': 'application/x-www-form-urlencoded' 
                 },
        jar: newJar // a new cookie jar
    }, function(error, response, body){
        if(error){
            console.log(error);
            return;
        }
        if (response.statusCode == 200) {
            var searchWord = noTestMonth;
            var idx = body.indexOf(searchWord) + searchWord.length + 1;
            if (idx >= searchWord.length + 1) {
                // if found noTestMonth
                return;
            };
            for(var testMonthsIdx in testMonths){
                searchWord = testMonths[testMonthsIdx];
                console.log('now searching month: '+searchWord);
                idx = body.indexOf(searchWord) + searchWord.length + 1;
                if(idx < searchWord.length + 1){
                    // no record found for this testMonth
                    console.log("no result found in " + searchWord + " for " + officeDict[office]);
                    continue;
                }

                var dmvDate = parseInt(body.substring(idx, idx + 2));

                if(noTestDates.indexOf(dmvDate) == -1 && ((testMonthsIdx == 1 && dmvDate <= endDate) || (testMonthsIdx == 0 && dmvDate >= startDate))) {
                    // now check the time
                    idx += (''+dmvDate).length + ', 2014 at '.length;
                    var dmvTime = body.substring(idx, idx + 1);

                    // cannot start in the morning too early
                    if (noStartHours.indexOf(dmvTime) != -1) {
                        console.log('hour not good: '+ officeDict[office]+" dmvDate: "+dmvDate +" hour: "+dmvTime);
                        continue;
                    };

                    // send the satisfied mail
                    sendMail(office, searchWord, dmvDate, 'DMV test date can be scheduled!');

                    console.log('satisfied result: '+ officeDict[office]+" dmvDate: "+dmvDate +" hour: "+dmvTime);

                    // now review the test
                    reviewTest(office, searchWord, dmvDate, dmvTime, newJar);
                }else{
                    console.log('unsatisfied result: '+ officeDict[office]+" dmvDate: "+dmvDate);
                }
            }
        };

    });
};

function confirmTest(office, month, dmvDate, dmvTime, cookiejar){
    request({
        uri: 'https://www.dmv.ca.gov/wasapp/foa/confirmDriveTest.do',
        method: 'POST',
        timeout: 10000,
        jar: cookiejar
    }, function(error, response, body){
        if (response.statusCode == 200) {
            console.log('successfully booked!');
            // send the confirmed mail
            sendMail(office, month, dmvDate, 'DMV test date has been confirmed! Date: '+ dmvDate +' hour: '+dmvTime);
console.log(cookiejar.getCookies('https://www.dmv.ca.gov'));
            process.exit();
        }else{
            console.log(error);
        }
    });	
}

function reviewTest(office, month, dmvDate, dmvTime, cookiejar){
    request({
        uri: 'https://www.dmv.ca.gov/wasapp/foa/reviewDriveTest.do',
    method: 'POST',
    timeout: 10000,
    jar: cookiejar
    }, function(error, response, body){
        if (response.statusCode == 200) {
            // confirm the book
            console.log('successfully reviewed');
            // send the reviewed mail
            sendMail(office, month, dmvDate, 'DMV test date has been reviewed! Date: '+ dmvDate +' hour: '+dmvTime);

            // now confirm the test
            confirmTest(office, month, dmvDate, dmvTime, cookiejar);
        }else{
            console.log(error);
        }
    });

}
// refresh every hour
var recursive = function () {
    for(var i = 0; i <officeList.length; i++){
        setTimeout(checkoffice, (Math.random() * (2000 - 1000) + 1000), officeList[i]);
    }
    setTimeout(recursive, Math.random() * 1000 + refreshInterval);
    console.log("time now: "+new Date());
};
recursive();

