module.exports = {
    mail: {
    	name: "Lyman's DMVChecker",
        from: {
            service: 'Gmail',
            auth: {
                user: '<yoursentemail>@gmail.com',
                pass: '<yoursentpassword>'
            }
        },
        to: [
            '<yourreceiveemail>@gmail.com'
        ]
    }
};
